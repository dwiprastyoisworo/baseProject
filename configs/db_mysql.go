package configs

import (
	"baseProject/models"
	"gorm.io/driver/mysql"
	_ "gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func DBInit() *gorm.DB {
	dsn := "root:root@tcp(127.0.0.1:3306)/golang_h8?charset=utf8&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(models.Person{})
	db.AutoMigrate(models.Order{})
	db.AutoMigrate(models.Item{})
	db.AutoMigrate(models.Product{})
	db.AutoMigrate(models.User{})
	db.AutoMigrate(models.Weather{})
	return db
}
