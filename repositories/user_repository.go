package repositories

import (
	"baseProject/helpers"
	"baseProject/models"
	"errors"
	"gorm.io/gorm"
)

type UserInterface interface {
	Save(user *models.User, db *gorm.DB) error
	FindByEmail(email string, db *gorm.DB) (models.User, error)
}

type UserRepository struct {
}

func NewUserRepository() UserInterface {
	return &UserRepository{}
}

func (repository UserRepository) Save(user *models.User, db *gorm.DB) error {
	result := db.Create(&user)
	return result.Error
}

func (repository UserRepository) FindByEmail(email string, db *gorm.DB) (models.User, error) {
	var user models.User
	result := db.Where("email =?", email).First(&user)
	helpers.ErrorPanic(result.Error)
	if result.RowsAffected == 0 {
		return user, errors.New("Data not found")
	}
	return user, nil
}
