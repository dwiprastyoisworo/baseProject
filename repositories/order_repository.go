package repositories

import (
	"baseProject/models"
	"errors"
	"gorm.io/gorm"
)

type OrderRepositoryInterface interface {
	Save(order *models.Order, db *gorm.DB) error
	Update(orderID int, order *models.Order, db *gorm.DB) error
	Delete(orderID int, db *gorm.DB) error
	FindById(orderID int, db *gorm.DB) (models.Order, error)
	FindAll(db *gorm.DB) ([]models.Order, error)
}

type OrderRepository struct {
}

func NewOrderRepository() OrderRepositoryInterface {
	return &OrderRepository{}
}

func (repository OrderRepository) Save(order *models.Order, db *gorm.DB) error {
	result := db.Create(&order)
	return result.Error
}

func (repository OrderRepository) Update(orderID int, order *models.Order, db *gorm.DB) error {
	result := db.Model(&models.Order{}).Where("id = ?", orderID).Updates(&order)
	return result.Error
}

func (repository OrderRepository) Delete(orderID int, db *gorm.DB) error {
	result := db.Select("Items").Delete(&models.Order{}, orderID)
	return result.Error
}

func (repository OrderRepository) FindById(orderID int, db *gorm.DB) (models.Order, error) {
	var order models.Order
	result := db.Where("id =?", orderID).Preload("Items").First(&order)
	if result.RowsAffected == 0 {
		return order, errors.New("Data not found")
	}
	return order, result.Error
}

func (repository OrderRepository) FindAll(db *gorm.DB) ([]models.Order, error) {
	var orders []models.Order
	result := db.Model(&models.Order{}).Preload("Items").Find(&orders)
	return orders, result.Error
}
