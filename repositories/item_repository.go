package repositories

import (
	"baseProject/helpers"
	"baseProject/models"
	"gorm.io/gorm"
)

type ItemRepositoryInterface interface {
	Update(itemId int, item *models.Item, db *gorm.DB)
}

type ItemRepository struct {
}

func NewItemRepository() ItemRepositoryInterface {
	return &ItemRepository{}
}

func (repository ItemRepository) Update(itemId int, item *models.Item, db *gorm.DB) {
	result := db.Model(&models.Item{}).Where("id = ?", itemId).Updates(&item)
	helpers.ErrorPanic(result.Error)
}
