package repositories

import (
	"baseProject/models"
	"gorm.io/gorm"
)

type WeatherRepositoryInterface interface {
	Get(db *gorm.DB) (models.Weather, error)
	InsertOrUpdate(weather models.Weather, db *gorm.DB) error
}

func (repository WeatherRepository) Get(db *gorm.DB) (models.Weather, error) {
	var weather models.Weather
	result := db.First(&weather)
	return weather, result.Error
}

func (repository WeatherRepository) InsertOrUpdate(weather models.Weather, db *gorm.DB) error {
	var wt models.Weather
	result := db.First(&wt)
	if result.RowsAffected == 0 {
		result = db.Create(&weather)
	} else {
		result = db.Model(&models.Weather{}).Where("id = ?", wt.ID).Updates(&weather)
	}
	return result.Error
}

type WeatherRepository struct {
}

func NewWeatherRepository() WeatherRepositoryInterface {
	return &WeatherRepository{}
}
