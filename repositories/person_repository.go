package repositories

import (
	"baseProject/helpers"
	"baseProject/models"
	"errors"
	"gorm.io/gorm"
)

type PersonRepositoryInterface interface {
	Save(person *models.Person, db *gorm.DB)
	Update(personId int, person *models.Person, db *gorm.DB)
	Delete(personId int, db *gorm.DB)
	FindById(personId int, db *gorm.DB) (models.Person, error)
	FindAll(db *gorm.DB) []models.Person
}

type PersonRepository struct {
}

func NewPersonRepository() PersonRepositoryInterface {
	return &PersonRepository{}
}

func (repository *PersonRepository) Save(person *models.Person, db *gorm.DB) {
	result := db.Create(&person)
	helpers.ErrorPanic(result.Error)
}

func (repository *PersonRepository) Update(personId int, person *models.Person, db *gorm.DB) {
	result := db.Model(&models.Person{}).Where("id = ?", personId).Updates(&person)
	helpers.ErrorPanic(result.Error)
}

func (repository *PersonRepository) Delete(personId int, db *gorm.DB) {
	result := db.Delete(&models.Person{}, personId)
	helpers.ErrorPanic(result.Error)
}

func (repository *PersonRepository) FindById(personId int, db *gorm.DB) (models.Person, error) {
	var person models.Person
	result := db.Where("id =?", personId).First(&person)
	helpers.ErrorPanic(result.Error)
	if result.RowsAffected == 0 {
		return person, errors.New("Data not found")
	}
	return person, nil
}

func (repository *PersonRepository) FindAll(db *gorm.DB) []models.Person {
	var persons []models.Person
	result := db.Find(&persons)
	helpers.ErrorPanic(result.Error)
	return persons
}
