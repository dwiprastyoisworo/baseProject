package models

import "gorm.io/gorm"

type Person struct {
	gorm.Model
	FirstName string
	LastName  string
}

type PersonCreateRequest struct {
	FirstName string
	LastName  string
}

type PersonUpdateRequest struct {
	ID        int
	FirstName string
	LastName  string
}

type PersonResponse struct {
	ID        int
	FirstName string
	LastName  string
}
