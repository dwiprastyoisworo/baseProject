package models

type Weather struct {
	GormModel
	Water int `gorm:"not null" json:"water"`
	Wind  int `gorm:"not null" json:"wind"`
}
