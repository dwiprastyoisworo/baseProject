package models

type Product struct {
	GormModel
	ProductName string `gorm:"not null" json:"product_name"`
	UserID      uint   `gorm:"not null" json:"user_id"`
}
