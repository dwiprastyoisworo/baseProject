package models

import (
	"gorm.io/gorm"
	"time"
)

type Item struct {
	ID          uint `gorm:"primaryKey"`
	ItemCode    string
	Description string
	Quantity    int
	OrderID     uint
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt `gorm:"index"`
}
