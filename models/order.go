package models

import (
	"gorm.io/gorm"
	"time"
)

type Order struct {
	ID           uint `gorm:"primaryKey"`
	CustomerName string
	OrderedAt    time.Time
	Items        []Item
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DeletedAt    gorm.DeletedAt `gorm:"index"`
}
