package models

type Brother struct {
	Uuid       string `json:"uuid"`
	First_Name string `json:"firstname"`
	Last_Name  string `json:"lastname"`
	Username   string `json:"username"`
	Phone      string `json:"phone"`
	Email      string `json:"email"`
}
