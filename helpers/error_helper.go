package helpers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func ErrorPanic(err error) {
	if err != nil {
		panic(err)
	}
}

func CatchErr(c *gin.Context) {
	if r := recover(); r != nil {
		result := gin.H{
			"result": r,
		}
		c.JSON(http.StatusBadRequest, result)
	}
}

func ErrorResponse(c *gin.Context, errorCode int, message string) {
	resultErr := gin.H{
		"error": message,
	}
	c.JSON(errorCode, resultErr)
	return
}
