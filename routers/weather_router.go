package routers

import (
	"baseProject/controllers"
	"baseProject/repositories"
	"baseProject/services"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func WeatherStartServer(db *gorm.DB, router *gin.RouterGroup) {
	weatherRepository := repositories.NewWeatherRepository()
	weatherService := services.NewWeatherService(weatherRepository, db)
	weatherController := controllers.NewWeatherController(weatherService)

	routerUser := router.Group("/weather")
	routerUser.GET("/", weatherController.Get)
}
