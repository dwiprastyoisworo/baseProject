package routers

import (
	"baseProject/controllers"
	"baseProject/middlewares"
	"baseProject/repositories"
	"baseProject/services"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func PersonStartServer(db *gorm.DB, router *gin.RouterGroup) {
	personRepository := repositories.NewPersonRepository()
	thirdPartyService := services.NewThirdPartyService()
	personService := services.NewPersonService(personRepository, thirdPartyService, db)
	personController := controllers.NewPersonController(personService)
	routerPerson := router.Group("/person").Use(middlewares.BasicAuth())

	routerPerson.GET("/", personController.FindAll)
	routerPerson.GET("/:id", personController.FindById)
	routerPerson.POST("/", personController.Create)
	routerPerson.PUT("/:id", personController.Update)
	routerPerson.DELETE("/:id", personController.Delete)
	routerPerson.GET("/brothers/:id", personController.FindPersonBrother)
}
