package routers

import (
	"baseProject/controllers"
	"baseProject/middlewares"
	"baseProject/repositories"
	"baseProject/services"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func OrderStartServer(db *gorm.DB, router *gin.RouterGroup) {
	orderRepository := repositories.NewOrderRepository()
	itemRepository := repositories.NewItemRepository()
	orderService := services.NewOrderService(orderRepository, itemRepository, db)
	orderController := controllers.NewOrderController(orderService)
	routerOrder := router.Group("/order").Use(middlewares.Authentication())
	routerOrder.GET("/", orderController.FindAll)
	routerOrder.POST("/", orderController.Create)
	routerOrder.PUT("/:id", orderController.Update)
	routerOrder.DELETE("/:id", orderController.Delete)
}
