package routers

import (
	"baseProject/controllers"
	"baseProject/repositories"
	"baseProject/services"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func UserStartServer(db *gorm.DB, router *gin.RouterGroup) {
	userRepository := repositories.NewUserRepository()
	userService := services.NewUserService(userRepository, db)
	userController := controllers.NewUserController(userService)

	routerUser := router.Group("/user")
	routerUser.POST("/register", userController.Register)
	routerUser.POST("/login", userController.UserLogin)
}
