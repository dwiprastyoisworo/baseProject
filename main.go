package main

import (
	"baseProject/configs"
	"baseProject/routers"
	"baseProject/schedule"
	"github.com/gin-gonic/gin"
	"github.com/go-co-op/gocron"
	"github.com/joho/godotenv"
	"log"
	"os"
	"time"
)

func main() {
	err := godotenv.Load()

	if err != nil {
		log.Fatal("Error loading .env file")
	}

	db := configs.DBInit()
	s := gocron.NewScheduler(time.UTC)
	schedule.WeatherScheduleRun(db, s)
	s.StartAsync()
	router := gin.Default()
	routerGroupApi := router.Group("/api")
	routers.PersonStartServer(db, routerGroupApi)
	routers.OrderStartServer(db, routerGroupApi)
	routers.UserStartServer(db, routerGroupApi)
	routers.WeatherStartServer(db, routerGroupApi)
	router.Run(os.Getenv("PORT"))

}
