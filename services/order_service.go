package services

import (
	"baseProject/models"
	"baseProject/repositories"
	"errors"
	"gorm.io/gorm"
)

type OrderServiceInterface interface {
	Create(request *models.Order) (models.Order, error)
	Update(orderId int, request *models.Order) models.Order
	Delete(orderId int)
	FindAll() []models.Order
}

type OrderService struct {
	orderRepository repositories.OrderRepositoryInterface
	itemRepository  repositories.ItemRepositoryInterface
	db              *gorm.DB
}

func NewOrderService(orderRepository repositories.OrderRepositoryInterface, itemRepository repositories.ItemRepositoryInterface, db *gorm.DB) *OrderService {
	return &OrderService{orderRepository: orderRepository, itemRepository: itemRepository, db: db}
}

func (service OrderService) Create(request *models.Order) (models.Order, error) {
	err := service.orderRepository.Save(request, service.db)
	if err != nil {
		return *request, errors.New("Create Order Fail")
	}
	return *request, nil
}

func (service OrderService) Update(orderId int, request *models.Order) models.Order {
	service.db.Transaction(func(tx *gorm.DB) error {
		service.orderRepository.FindById(orderId, tx)
		service.orderRepository.Update(orderId, request, tx)
		for _, x := range request.Items {
			service.itemRepository.Update(int(x.ID), &x, tx)
		}
		return nil
	})
	return *request
}

func (service OrderService) Delete(orderId int) {
	service.orderRepository.Delete(orderId, service.db)
}

func (service OrderService) FindAll() []models.Order {
	orders, _ := service.orderRepository.FindAll(service.db)
	return orders
}
