package services

import (
	"baseProject/models"
	"baseProject/repositories"
	"encoding/json"
	"errors"
	"fmt"
	"gorm.io/gorm"
	"io/ioutil"
)

type WeatherService struct {
	weatherRepository repositories.WeatherRepositoryInterface
	db                *gorm.DB
}

func NewWeatherService(weatherRepository repositories.WeatherRepositoryInterface, db *gorm.DB) WeatherServiceInterface {
	return &WeatherService{weatherRepository: weatherRepository, db: db}
}

type WeatherServiceInterface interface {
	Create() error
	Get() (string, string, error)
}

func (service WeatherService) Create() error {
	type Payload struct {
		Status models.Weather `json:"status"`
	}

	content, err := ioutil.ReadFile("./weather.json")

	if err != nil {
		errors.New("Error opening file")
	}
	var data Payload
	err = json.Unmarshal(content, &data)

	if err != nil {
		errors.New("Error Unmarshal()")
	}

	fmt.Println("schedule running")

	return service.weatherRepository.InsertOrUpdate(data.Status, service.db)
}

func (service WeatherService) Get() (string, string, error) {
	var weather models.Weather
	var resultWater string
	var resultWind string
	weather, err := service.weatherRepository.Get(service.db)
	water := weather.Water
	wind := weather.Wind
	if water < 5 {
		resultWater = "aman"
	} else if water >= 6 && water <= 8 {
		resultWater = "siaga"
	} else {
		resultWater = "bahaya"
	}

	if wind < 6 {
		resultWind = "aman"
	} else if wind >= 7 && wind <= 15 {
		resultWind = "siaga"
	} else {
		resultWind = "bahaya"
	}

	return resultWater, resultWind, err
}
