package services

import (
	"baseProject/models"
	"baseProject/repositories"
	"gorm.io/gorm"
)

type UserInterface interface {
	Create(user *models.User) (models.User, error)
	FindByEmail(email string) (models.User, error)
}

type UserService struct {
	userRepository repositories.UserInterface
	db             *gorm.DB
}

func NewUserService(userRepository repositories.UserInterface, db *gorm.DB) UserInterface {
	return &UserService{userRepository: userRepository, db: db}
}

func (service UserService) Create(user *models.User) (models.User, error) {
	err := service.userRepository.Save(user, service.db)
	return *user, err
}

func (service UserService) FindByEmail(email string) (models.User, error) {
	user, err := service.userRepository.FindByEmail(email, service.db)
	return user, err
}
