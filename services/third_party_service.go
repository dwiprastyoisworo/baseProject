package services

import (
	"io/ioutil"
	"net/http"
)

type ThirdPartyServiceInterface interface {
	get(url string) string
	post(map[string]interface{})
}

type ThirdPartyService struct {
}

func NewThirdPartyService() ThirdPartyServiceInterface {
	return &ThirdPartyService{}
}

func (service ThirdPartyService) get(url string) string {
	client := &http.Client{}
	httpRes, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		panic("request http error")
	}

	doRes, err := client.Do(httpRes)
	if err != nil {
		panic("error client do")
	}

	defer doRes.Body.Close()
	body, err := ioutil.ReadAll(doRes.Body)
	if err != nil {
		panic("error readAll")
	}

	return string(body)
}

func (service ThirdPartyService) post(m map[string]interface{}) {
	//TODO implement me
	panic("implement me")
}
