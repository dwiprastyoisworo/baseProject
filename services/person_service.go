package services

import (
	"baseProject/helpers"
	"baseProject/models"
	"baseProject/repositories"
	"encoding/json"
	"gorm.io/gorm"
	"os"
)

type PersonServiceInterface interface {
	Create(request *models.PersonCreateRequest) models.PersonResponse
	Update(request *models.PersonUpdateRequest) models.PersonResponse
	Delete(personId int)
	FindById(personId int) models.PersonResponse
	FindAll() []models.PersonResponse
	FindPersonBrother(personId int) map[string]interface{}
}

type PersonService struct {
	personRepository  repositories.PersonRepositoryInterface
	thirdPartyService ThirdPartyServiceInterface
	db                *gorm.DB
}

func NewPersonService(personRepository repositories.PersonRepositoryInterface, thirdPartyService ThirdPartyServiceInterface, db *gorm.DB) *PersonService {
	return &PersonService{personRepository: personRepository, thirdPartyService: thirdPartyService, db: db}
}

func (service *PersonService) Create(request *models.PersonCreateRequest) models.PersonResponse {
	person := models.Person{
		FirstName: request.FirstName,
		LastName:  request.LastName,
	}
	service.personRepository.Save(&person, service.db)
	return models.PersonResponse{
		ID:        int(person.ID),
		FirstName: person.FirstName,
		LastName:  person.LastName,
	}
}

func (service *PersonService) Update(request *models.PersonUpdateRequest) models.PersonResponse {
	person := models.Person{
		FirstName: request.FirstName,
		LastName:  request.LastName,
	}
	_, err := service.personRepository.FindById(request.ID, service.db)
	helpers.ErrorPanic(err)
	service.personRepository.Update(request.ID, &person, service.db)
	return models.PersonResponse{
		ID:        int(person.ID),
		FirstName: person.FirstName,
		LastName:  person.LastName,
	}
}

func (service *PersonService) Delete(personId int) {
	service.personRepository.Delete(personId, service.db)
}

func (service *PersonService) FindById(personId int) models.PersonResponse {
	person, err := service.personRepository.FindById(personId, service.db)
	helpers.ErrorPanic(err)
	return models.PersonResponse{
		ID:        int(person.ID),
		FirstName: person.FirstName,
		LastName:  person.LastName,
	}
}

func (service *PersonService) FindAll() []models.PersonResponse {
	var persons []models.PersonResponse
	person := service.personRepository.FindAll(service.db)
	for _, x := range person {
		persons = append(persons, models.PersonResponse{
			FirstName: x.FirstName,
			LastName:  x.LastName,
		})
	}
	return persons
}

func (service *PersonService) FindPersonBrother(personId int) map[string]interface{} {
	person, err := service.personRepository.FindById(personId, service.db)
	helpers.ErrorPanic(err)
	personResponse := models.PersonResponse{
		ID:        int(person.ID),
		FirstName: person.FirstName,
		LastName:  person.LastName,
	}
	url := os.Getenv("SITE_URL")
	apiKey := os.Getenv("SITE_API_KEY")
	res := service.thirdPartyService.get(url + "/data.php?qty=1&apikey=" + apiKey)
	var brother map[string]interface{}
	var errJson = json.Unmarshal([]byte(res), &brother)

	if errJson != nil {
		panic(errJson)
	}

	result := map[string]interface{}{
		"person":  personResponse,
		"brother": brother["result"],
	}

	return result
}
