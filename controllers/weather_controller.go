package controllers

import (
	"baseProject/helpers"
	"baseProject/services"
	"github.com/gin-gonic/gin"
	"net/http"
)

type WeatherController struct {
	weatherService services.WeatherServiceInterface
}

func NewWeatherController(weatherService services.WeatherServiceInterface) WeatherControllerInterface {
	return &WeatherController{weatherService: weatherService}
}

type WeatherControllerInterface interface {
	Get(c *gin.Context)
}

func (controller WeatherController) Get(c *gin.Context) {
	water, wind, err := controller.weatherService.Get()

	if err != nil {
		helpers.ErrorResponse(c, http.StatusInternalServerError, "Error get Weather")
	}

	result := gin.H{
		"water": water,
		"wind":  wind,
	}
	c.JSON(http.StatusOK, result)
}
