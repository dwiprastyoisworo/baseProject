package controllers

import (
	"baseProject/helpers"
	"baseProject/models"
	"baseProject/services"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type OrderControllerInterface interface {
	Create(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
	FindAll(c *gin.Context)
}

func NewOrderController(orderService services.OrderServiceInterface) OrderControllerInterface {
	return &OrderController{orderService: orderService}
}

type OrderController struct {
	orderService services.OrderServiceInterface
}

func (controller OrderController) Create(c *gin.Context) {
	defer helpers.CatchErr(c)
	var order models.Order

	if err := c.ShouldBindJSON(&order); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	data, err := controller.orderService.Create(&order)

	if err != nil {
		helpers.ErrorResponse(c, http.StatusInternalServerError, "Error Create Order")
	}

	result := gin.H{
		"result": data,
	}
	c.JSON(http.StatusOK, result)
}

func (controller OrderController) Update(c *gin.Context) {
	defer helpers.CatchErr(c)
	var order models.Order
	id := c.Param("id")
	intVar, err := strconv.Atoi(id)
	helpers.ErrorPanic(err)

	if err := c.ShouldBindJSON(&order); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	data := controller.orderService.Update(intVar, &order)
	result := gin.H{
		"result": data,
	}
	c.JSON(http.StatusOK, result)

}

func (controller OrderController) Delete(c *gin.Context) {
	defer helpers.CatchErr(c)
	id := c.Param("id")
	intVar, err := strconv.Atoi(id)
	helpers.ErrorPanic(err)
	controller.orderService.Delete(intVar)
	result := gin.H{
		"result": "successfully deleted",
	}
	c.JSON(http.StatusOK, result)
}

func (controller OrderController) FindAll(c *gin.Context) {
	defer helpers.CatchErr(c)
	orderResponse := controller.orderService.FindAll()
	result := gin.H{
		"result": orderResponse,
	}
	c.JSON(http.StatusOK, result)
}
