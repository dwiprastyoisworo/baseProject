package controllers

import (
	"baseProject/helpers"
	"baseProject/models"
	"baseProject/services"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type PersonControllerInterface interface {
	Create(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
	FindById(c *gin.Context)
	FindAll(c *gin.Context)
	FindPersonBrother(c *gin.Context)
}

type PersonController struct {
	PersonService services.PersonServiceInterface
}

func NewPersonController(personService services.PersonServiceInterface) PersonControllerInterface {
	return &PersonController{
		PersonService: personService,
	}
}

func (controller PersonController) Create(c *gin.Context) {
	defer helpers.CatchErr(c)
	var personRequest models.PersonCreateRequest
	personRequest.FirstName = c.PostForm("first_name")
	personRequest.LastName = c.PostForm("last_name")
	controller.PersonService.Create(&personRequest)
	result := gin.H{
		"result": personRequest,
	}
	c.JSON(http.StatusOK, result)
}

func (controller PersonController) Update(c *gin.Context) {
	defer helpers.CatchErr(c)
	var personRequest models.PersonUpdateRequest
	id := c.Param("id")
	intVar, err := strconv.Atoi(id)
	helpers.ErrorPanic(err)
	personRequest.ID = intVar
	personRequest.FirstName = c.PostForm("first_name")
	personRequest.LastName = c.PostForm("last_name")
	controller.PersonService.Update(&personRequest)
	result := gin.H{
		"result": personRequest,
	}
	c.JSON(http.StatusOK, result)
}

func (controller PersonController) Delete(c *gin.Context) {
	defer helpers.CatchErr(c)
	id := c.Param("id")
	intVar, err := strconv.Atoi(id)
	helpers.ErrorPanic(err)
	controller.PersonService.Delete(intVar)
	result := gin.H{
		"result": "successfully updated",
	}
	c.JSON(http.StatusOK, result)
}

func (controller PersonController) FindById(c *gin.Context) {
	defer helpers.CatchErr(c)
	id := c.Param("id")
	intVar, err := strconv.Atoi(id)
	helpers.ErrorPanic(err)
	personResponse := controller.PersonService.FindById(intVar)
	result := gin.H{
		"result": personResponse,
	}
	c.JSON(http.StatusOK, result)

}

func (controller PersonController) FindAll(c *gin.Context) {
	defer helpers.CatchErr(c)
	personResponse := controller.PersonService.FindAll()
	result := gin.H{
		"result": personResponse,
	}
	c.JSON(http.StatusOK, result)
}

func (controller PersonController) FindPersonBrother(c *gin.Context) {
	defer helpers.CatchErr(c)
	id := c.Param("id")
	intVar, err := strconv.Atoi(id)
	helpers.ErrorPanic(err)
	personResponse := controller.PersonService.FindPersonBrother(intVar)
	result := gin.H{
		"result": personResponse,
	}
	c.JSON(http.StatusOK, result)
}
