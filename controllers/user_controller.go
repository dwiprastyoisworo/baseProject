package controllers

import (
	"baseProject/helpers"
	"baseProject/models"
	"baseProject/services"
	"github.com/gin-gonic/gin"
	"net/http"
)

type UserInterface interface {
	Register(c *gin.Context)
	UserLogin(c *gin.Context)
}

type UserController struct {
	userService services.UserInterface
}

func NewUserController(userService services.UserInterface) UserInterface {
	return &UserController{userService: userService}
}

func (controller UserController) Register(c *gin.Context) {
	var user models.User

	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	data, err := controller.userService.Create(&user)
	if err != nil {
		helpers.ErrorResponse(c, http.StatusInternalServerError, "Error Register")
	}

	result := gin.H{
		"result": data.UserToUser(),
	}
	c.JSON(http.StatusOK, result)
}

func (controller UserController) UserLogin(c *gin.Context) {
	var user models.User
	password := ""

	if err := c.ShouldBindJSON(&user); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	password = user.Password

	userResponse, err := controller.userService.FindByEmail(user.Email)

	if err != nil {
		helpers.ErrorResponse(c, http.StatusUnauthorized, "Invalid email / password")
	}

	comparePass := helpers.ComparePass([]byte(userResponse.Password), []byte(password))

	if !comparePass {
		helpers.ErrorResponse(c, http.StatusUnauthorized, "Invalid email / password")
	}

	token, err := helpers.GenerateToken(userResponse.ID, userResponse.Email)

	if err != nil {
		helpers.ErrorResponse(c, http.StatusInternalServerError, "Error generate token")
	}

	result := gin.H{
		"token": token,
	}
	c.JSON(http.StatusOK, result)

}
