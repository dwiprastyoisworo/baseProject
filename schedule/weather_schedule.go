package schedule

import (
	"baseProject/repositories"
	"baseProject/services"
	"github.com/go-co-op/gocron"
	"gorm.io/gorm"
)

func WeatherScheduleRun(db *gorm.DB, scheduler *gocron.Scheduler) {
	weatherRepository := repositories.NewWeatherRepository()
	weatherService := services.NewWeatherService(weatherRepository, db)
	scheduler.Every(5).Second().Do(func() { weatherService.Create() })
}
