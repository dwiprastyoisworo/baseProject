package middlewares

import (
	"baseProject/helpers"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
)

func BasicAuth() gin.HandlerFunc {
	return func(context *gin.Context) {
		user, password, hasAuth := context.Request.BasicAuth()
		isValid := hasAuth && user == os.Getenv("BASIC_AUTH_USER") && password == os.Getenv("BASIC_AUTH_PASSWORD")
		if !isValid {
			context.Abort()
			result := gin.H{
				"result": "unauthorized access",
			}
			context.JSON(http.StatusUnauthorized, result)
		}
	}
}

func Authentication() gin.HandlerFunc {
	return func(c *gin.Context) {
		verifyToken, err := helpers.VerifyToken(c)
		_ = verifyToken

		if err != nil {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"error":   "Unauthenticated",
				"message": err.Error(),
			})
		}
		c.Set("userData", verifyToken)
		c.Next()
	}
}
